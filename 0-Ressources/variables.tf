variable "resource_group_name_Linux_name" {
  default = "Linux-resources"
}

variable "resource_group_name_Linux_location" {
  default = "eastus"
}

variable "resource_group_name_Windows_name" {
  default = "Windows-resources"
}

variable "resource_group_name_Windows_location" {
  default = "eastus"
}

#variable topic_name {}