# Configure the Azure provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.98.0"
    }
  }

  required_version = ">= 1.1.0"
}

provider "azurerm" {
  features {}

}


# Create a resource group
resource "azurerm_resource_group" "Windows" {
  name     = var.resource_group_name_Windows_name
  location = var.resource_group_name_Windows_location
}


resource "azurerm_resource_group" "Linux" {
  name     = var.resource_group_name_Linux_name
  location = var.resource_group_name_Linux_location
}