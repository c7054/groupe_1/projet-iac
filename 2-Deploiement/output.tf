output "publicip_win" {
  value = azurerm_windows_virtual_machine.Windows.public_ip_address
}

output "publicip_lin" {
  value = azurerm_linux_virtual_machine.Linux.public_ip_address
}

#output "Id" {
#  value = resource.azurerm_shared_image.Linux-Image.id
#}
