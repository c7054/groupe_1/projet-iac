# Configure the Azure provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.98.0"
    }
  }

  required_version = ">= 1.1.0"
}

provider "azurerm" {
  features {}

}

resource "azurerm_public_ip" "Linux" {
  name                = "Linux-PublicIp"
  resource_group_name = var.resource_group_name_Linux_name
  location            = var.resource_group_name_Linux_location
  allocation_method   = "Dynamic"
}

resource "azurerm_virtual_network" "Linux" {
  name                = "Linux-network"
  address_space       = ["10.0.0.0/16"]
  location            = "eastus"
  resource_group_name = var.resource_group_name_Linux_name
}

resource "azurerm_subnet" "internal" {
  name                 = "internal"
  resource_group_name  = var.resource_group_name_Linux_name
  virtual_network_name = azurerm_virtual_network.Linux.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_network_interface" "Linux" {
  name                = "Linux-nic"
  resource_group_name = var.resource_group_name_Linux_name
  location            = "eastus"

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.internal.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.Linux.id
  }
}


resource "azurerm_linux_virtual_machine" "Linux" {
  name                            = "Linux-vm"
  resource_group_name             = var.resource_group_name_Linux_name
  location                        = "eastus"
  size                            = "Standard_B1ms"
  admin_username                  = "adminuser"
  admin_password                  = "P@ssw0rd1234!"
  custom_data                     = base64encode("Hello World!")
  disable_password_authentication = false
  network_interface_ids = [
    azurerm_network_interface.Linux.id,
  ]

  source_image_id = "/subscriptions/cbbb5627-ca60-48c8-af99-38dc687232df/resourceGroups/Linux-resources/providers/Microsoft.Compute/images/Linux-16-04"

  #source_image_reference {
  #  publisher = "Canonical"
  #  offer     = "UbuntuServer"
  #  sku       = "16.04-LTS"
  #  version   = "latest"
  #}

   tags = {
    Scope          = "VM"
    Env            = "Linux"

  }

  os_disk {
    storage_account_type = "Premium_LRS"
    caching              = "ReadWrite"
  }
}

resource "azurerm_public_ip" "Windows" {
  name                = "Windows-PublicIp"
  resource_group_name = var.resource_group_name_Windows_name
  location            = var.resource_group_name_Windows_location
  allocation_method   = "Dynamic"

}


resource "azurerm_network_interface" "Windows" {
  name                = "Windows-nic"
  resource_group_name = var.resource_group_name_Windows_name
  location            = "eastus"

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.internal.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.Windows.id
  }
}


resource "azurerm_windows_virtual_machine" "Windows" {
  name                = "Windows-vm"
  resource_group_name = var.resource_group_name_Windows_name
  location            = "eastus"
  size                = "Standard_B1ms"
  admin_username      = "admingrp1"
  admin_password      = "5Gpwf4U73fq"
  license_type        = "Windows_Server"
  network_interface_ids = [
    azurerm_network_interface.Windows.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }
  tags = {
    Scope          = "VM"
    Env            = "Windows"

  }
 

}