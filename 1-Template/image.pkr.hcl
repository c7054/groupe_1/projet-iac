source "azure-arm" "Linux-16-04" {
  tenant_id = "190ce420-b157-44ae-bc2f-69563baa5a3b"
  subscription_id = "cbbb5627-ca60-48c8-af99-38dc687232df"
  
  managed_image_name = "Linux-16-04"
  managed_image_resource_group_name = "Linux-resources"

  os_type = "Linux"
  image_publisher = "Canonical"
  image_offer = "UbuntuServer"
  image_sku = "16.04-LTS"

  location = "East US"
  vm_size = "Standard_DS2_v2"

  azure_tags = {
    dept = "Engineering"
    task = "Image deployement"
  }
}

build {
  sources = ["sources.azure-arm.Linux-16-04"]

  provisioner "shell" {
    script = "./3-Provisionnement/scripts/deploy_ansible.sh"
  }

  provisioner "ansible-local" {
    playbook_file = "./3-Provisionnement/ansible/playbook/wordpress.yml"
    role_paths = [
      "./3-Provisionnement/ansible/playbook/roles/apache",
      "./3-Provisionnement/ansible/playbook/roles/php",
      "./3-Provisionnement/ansible/playbook/roles/mysql",
      "./3-Provisionnement/ansible/playbook/roles/wordpress"
    ]
  }
  #post-processors "artifact_id" {
  #  output = "Variableid.tf"
  #  custom_data = {
  #    artifact_id = image_id
  #      }
  #  }
}