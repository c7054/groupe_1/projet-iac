#!/bin/bash -e

sudo apt-get install mariadb-server php libapache2-mod-php php-mysql php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip -y
cd /tmp
sudo wget https://fr.wordpress.org/latest-fr_FR.tar.gz
tar xvf latest-fr_FR.tar.gz
sudo mv wordpress /var/www/
sudo chown -R www-data:www-data /var/www
sudo chmod -R 755 /var/www/html/
sudo mv /tmp/wordpress.conf /etc/apache2/sites-available/
sudo a2dissite 000-default.conf
sudo a2ensite wordpress
sudo service apache2 reload
